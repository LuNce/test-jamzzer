package jamzzer.register.event;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.admin.AdminEvent;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class UserRegisterEventListenerProvider implements EventListenerProvider {

    /**
     * when a user verify it's email, we notify ms-user with the user-id in body
     * @param event
     */
    private void onVerifyEmail(Event event) {
        // get user informations
//        System.out.println("B");
//        Map<String, String> details = event.getDetails();
//        String us_uuid = event.getUserId();
//        String us_mail = details.get("email");
//        String us_username = details.get("username");

        try {
            System.out.println("C");
            // establish connection
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUri("amqps://iqsmylqu:Ly5GgzKEKtqqDcTxOsS310zVZy93NonJ@albatross.rmq.cloudamqp.com/iqsmylqu");
            Connection conn = factory.newConnection();
            Channel channel = conn.createChannel();
            channel.basicPublish("", "jamzzer-dev", null,
                    "message".getBytes((StandardCharsets.UTF_8)));

            System.out.println("D");
            // create JSON
            JsonObject userObj = Json.createObjectBuilder()
                    .add("us_uuid", "us_uuid")
                    .add("us_username", "us_username")
                    .add("us_email", "us_mail")
                    .build();

            System.out.println("E");

            // generate json
            StringWriter stringWriter = new StringWriter();
            JsonWriter writer = Json.createWriter(stringWriter);
            writer.writeObject(userObj);
            writer.close();
            System.out.println(stringWriter.getBuffer().toString());
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException | TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEvent(Event event) {
        if (true)
            onVerifyEmail(event);
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {

    }

    @Override
    public void close() {

    }
}
